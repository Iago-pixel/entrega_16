from flask import request, current_app, jsonify
from app.models.categories_model import CategoryModel

from sqlalchemy.exc import IntegrityError
from psycopg2.errors import UniqueViolation
from sqlalchemy.orm.exc import UnmappedInstanceError


def create_category():
    data = request.json
    try:
        new_category = CategoryModel(**data)

        current_app.db.session.add(new_category)
        current_app.db.session.commit()

        return jsonify(new_category), 201
    except IntegrityError as e:
        if type(e.orig) == UniqueViolation:
            return jsonify({'msg': 'Category already exists!'}), 409


def update_category(category_id: int):
    data = request.json
    try:
        category = CategoryModel.query.get(category_id)

        for key, value in data.items():
            setattr(category, key, value)

        current_app.db.session.add(category)
        current_app.db.session.commit()

        return jsonify(category), 200
    except AttributeError:
        return jsonify({'msg': 'category not found!'}), 404


def delete_category(category_id: int):
    try:
        query = CategoryModel.query.get(category_id)

        current_app.db.session.delete(query)
        current_app.db.session.commit()

        return '', 204
    except UnmappedInstanceError:
        return jsonify({'msg': 'category not found!'}), 404
