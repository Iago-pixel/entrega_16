from flask import request, current_app, jsonify
from app.models.task_model import TaskModel
from app.models.tasks_categories_model import TasksCategoriesModel
from app.models.categories_model import CategoryModel

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import UnmappedInstanceError
from psycopg2.errors import NotNullViolation, UniqueViolation


def create_task():
    data: dict = request.json
    try:
        importance = data['importance']
        urgency = data['urgency']

        if importance == 1 and urgency == 1:
            data['eisenhower_id'] = 1
        if importance == 1 and urgency == 2:
            data['eisenhower_id'] = 2
        if importance == 2 and urgency == 1:
            data['eisenhower_id'] = 3
        if importance == 2 and urgency == 2:
            data['eisenhower_id'] = 4

        categories = data.pop('categories')

        task = TaskModel(**data)

        current_app.db.session.add(task)
        current_app.db.session.commit()

        for category in categories:

            category_filted = CategoryModel.query.filter(
                CategoryModel.name == category['name']).one_or_none()

            if category_filted == None:
                data_category = {'name': category['name'], 'description': ''}

                categoty_insert = CategoryModel(**data_category)

                current_app.db.session.add(categoty_insert)
                current_app.db.session.commit()

                category_filted = CategoryModel.query.filter(
                    CategoryModel.name == category['name']).one_or_none()

                data_task_category = {'task_id': task.task_id,
                                      'category_id': category_filted.category_id}

            else:
                data_task_category = {'task_id': task.task_id,
                                      'category_id': category_filted.category_id}

            task_category = TasksCategoriesModel(**data_task_category)

            current_app.db.session.add(task_category)
            current_app.db.session.commit()

        return jsonify({
            'id': task.task_id,
            'name': task.name,
            'description': task.description,
            'duration': task.duration,
            'eisenhower_classification': task.eisenhower.type,
            'importance': task.importance,
            'urgency': task.urgency,
            'categories': categories
        }), 201

    except IntegrityError as e:
        if type(e.orig) == NotNullViolation:
            return jsonify({
                'error': {
                    'valid_options': {
                        'importance': [
                            1,
                            2
                        ],
                        'urgency': [
                            1,
                            2
                        ]
                    },
                    'receuved_options': {
                        'importance': data['importance'],
                        'urgency': data['urgency']
                    }
                }
            }), 404
        if type(e.orig) == UniqueViolation:
            return jsonify({'msg': 'task already exists'}), 409
        return {'msg': str(e)}, 500


def update_task(task_id: int):
    data = request.json
    try:
        task = TaskModel.query.get(task_id)

        for key, value in data.items():
            setattr(task, key, value)

        if 'importance' in data or 'urgency' in data:
            importance = task.importance
            urgency = task.urgency

            if importance == 1 and urgency == 1:
                eisenhower_id = 1
            if importance == 1 and urgency == 2:
                eisenhower_id = 2
            if importance == 2 and urgency == 1:
                eisenhower_id = 3
            if importance == 2 and urgency == 2:
                eisenhower_id = 4

            setattr(task, 'eisenhower_id', eisenhower_id)

        current_app.db.session.add(task)
        current_app.db.session.commit()

        return jsonify({
            'id': task.task_id,
            'name': task.name,
            'description': task.description,
            'duration': task.duration,
            'eisenhower_classification': task.eisenhower.type
        }), 201
    except AttributeError:
        return jsonify({'msg': 'task not found!'}), 404


def delete_task(task_id: int):
    try:
        query = TaskModel.query.get(task_id)

        current_app.db.session.delete(query)
        current_app.db.session.commit()

        return '', 204
    except UnmappedInstanceError:
        return jsonify({'msg': 'task not found!'}), 404
