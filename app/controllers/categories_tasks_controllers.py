from flask import jsonify

from app.models.categories_model import CategoryModel
from app.models.eisenhower_model import EisenhowerModel
from app.models.task_model import TaskModel


def show_all():
    categories_data = CategoryModel.query.all()

    categories = []

    for category in categories_data:
        tasks = []

        for task in category.tasks:
            importance = task.importance
            urgency = task.urgency

            if importance == 1 and urgency == 1:
                eisenhower_id = 1
            if importance == 1 and urgency == 2:
                eisenhower_id = 2
            if importance == 2 and urgency == 1:
                eisenhower_id = 3
            if importance == 2 and urgency == 2:
                eisenhower_id = 4

            eisenhower = EisenhowerModel.query.get(eisenhower_id)

            task_treated = {'id': task.task_id,
                            'name': task.name, 'description': task.description, 'priority': eisenhower.type}

            tasks.append(task_treated)

        category_treated = {'id': category.category_id,
                            'name': category.name, 'description': category.description, 'tasks': tasks}
        categories.append(category_treated)

    return jsonify(categories), 200
