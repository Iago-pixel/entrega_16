from flask import Flask


def init_app(app: Flask):
    from app.routes.category_blueprint import bp as bp_categories
    app.register_blueprint(bp_categories)

    from app.routes.task_blueprint import bp as bp_tasks
    app.register_blueprint(bp_tasks)

    from app.routes.categories_tasks_blueprint import bp as bp_categories_tasks
    app.register_blueprint(bp_categories_tasks)
