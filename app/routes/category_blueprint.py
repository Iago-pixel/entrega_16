from flask import Blueprint
from app.controllers.categories_contollers import create_category, update_category, delete_category

bp = Blueprint('bp_categories', __name__, url_prefix='/category')

bp.post('')(create_category)
bp.patch('/<int:category_id>')(update_category)
bp.delete('/<int:category_id>')(delete_category)
