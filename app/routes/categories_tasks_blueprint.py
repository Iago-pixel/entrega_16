from flask import Blueprint
from app.controllers.categories_tasks_controllers import show_all

bp = Blueprint('bp_categories_tasks', __name__)

bp.get('/')(show_all)
