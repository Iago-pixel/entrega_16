from app.configs.database import db


class EisenhowerModel(db.Model):
    __tablename__ = 'eisenhowers'

    eisenhower_id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(100))

    # tasks = db.relationship('tasks', backref='eisenhower', uselist=True)
