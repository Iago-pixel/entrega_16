from app.configs.database import db

from app.exc.ValidationsErrors import EisenhowerScoreError


class TaskModel(db.Model):
    __tablename__ = 'tasks'

    task_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    description = db.Column(db.String())
    duration = db.Column(db.Integer)
    importance = db.Column(db.Integer)
    urgency = db.Column(db.Integer)

    eisenhower_id = db.Column(db.Integer, db.ForeignKey(
        'eisenhowers.eisenhower_id'), nullable=False)

    eisenhower = db.relationship('EisenhowerModel')
