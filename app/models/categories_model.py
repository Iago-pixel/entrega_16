from dataclasses import dataclass

from sqlalchemy.orm import backref
from app.configs.database import db
# from app.models.tasks_categories_model import tasks_categories


@dataclass
class CategoryModel(db.Model):
    category_id: int
    name: str
    description: str

    __tablename__ = 'categories'

    category_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    description = db.Column(db.String())

    tasks = db.relationship(
        'TaskModel', secondary='tasks_categories', backref='categories', uselist=True)
