# POST /category

## body request

**the category cannot exist, otherwise a 409 error will occur**

```
{
  "name": "Trabalho",
	"description": "tudo que preciso fazer no trabalho"
}
```

## response

```
{
  "category_id": 1,
  "name": "Trabalho",
  "description": "tudo que preciso fazer no trabalho"
}
```

status code 201

# PATCH /category/id

**able to update any data, individually or all at once**

## body request

```
{
	"name": "lazer",
	"description": "tudo que quero fazer fora do trabalho"
}
```

## response

```
{
  "category_id": 1,
  "name": "lazer",
  "description": "tudo que quero fazer fora do trabalho"
}
```

status code 200

# DELETE /category/id

## response

status code 204

# POST /task

## body request

```
{
  "name": "escrever planilha",
	"description": "organizar dados na planilha",
	"duration": 180,
	"importance": 1,
	"urgency": 1,
	"categories": [
		{"name": "Importante"},
		{"name": "Trabalho"}
	]
}
```

- **if the category passed in categories does not exist, it will be created**
- **if values greater than 1 or 2 are passed in keys importance or urgency, a 404 error will occur**
- **there cannot be two tasks with the same name, in which case error 409 will occur**

## response

```
{
  "id": 13,
  "name": "escrever planilha",
  "description": "organizar dados na planilha",
  "duration": 180,
  "eisenhower_classification": "Do It First",
  "importance": 1,
  "urgency": 1,
  "categories": [
    {
      "name": "Importante"
    },
    {
      "name": "Trabalho"
    }
  ]
}
```

# PATCH /task/id

**capaz de atualizar qualquer dado, individualmente ou todos de uma vez**

## body request

```
{
	"importance": 2,
	"description": "atualizar dados da tabela"
}
```

## response

```
{
  "id": 13,
  "name": "escrever planilha",
  "description": "atualizar dados da tabela",
  "duration": 180,
  "eisenhower_classification": "Delegate It"
}
```

status code 201

# DELETE /task/id

## response

status code 204

# GET /

**all categories with their respective tasks**

## response

```
[
  {
    "id": 2,
    "name": "Casa",
    "description": "",
    "tasks": [
      {
        "id": 14,
        "name": "jogar videogame",
        "description": "zerar os jogos da minha lista de jogos",
        "priority": "Schedule It"
      }
    ]
  },
  {
    "id": 3,
    "name": "Trabalho",
    "description": "",
    "tasks": [
      {
        "id": 13,
        "name": "escrever planilha",
        "description": "atualizar dados da tabela",
        "priority": "Delegate It"
      }
    ]
  },
  {
    "id": 4,
    "name": "Importante",
    "description": "",
    "tasks": [
      {
        "id": 13,
        "name": "escrever planilha",
        "description": "atualizar dados da tabela",
        "priority": "Delegate It"
      }
    ]
  },
  {
    "id": 5,
    "name": "Lazer",
    "description": "",
    "tasks": [
      {
        "id": 14,
        "name": "jogar videogame",
        "description": "zerar os jogos da minha lista de jogos",
        "priority": "Schedule It"
      }
    ]
  }
]
```

status code 200
